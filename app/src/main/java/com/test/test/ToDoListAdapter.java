package com.test.test;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.test.test.model.ToDoModel;

import java.util.ArrayList;


public class ToDoListAdapter extends RecyclerView.Adapter<ToDoListAdapter.ViewHolder> implements Filterable {

    private ArrayList<ToDoModel> toDoList;
    private ArrayList<ToDoModel> filterList;
    private Context context;
    private Filter fRecords;
    private int checkedPos = -1;

    @Override
    public Filter getFilter() {
        if (fRecords == null) {
            fRecords = new RecordFilter();
        }
        return fRecords;
    }

    public interface DeleteButtonClickListener {
        void onDeleteClick(ToDoModel toDoModel);
    }

    private DeleteButtonClickListener deleteButtonClickListener;

    public ToDoListAdapter(Context context, DeleteButtonClickListener deleteButton) {
        this.context = context;
        toDoList = new ArrayList<>();
        filterList = new ArrayList<>();
        this.deleteButtonClickListener = deleteButton;
    }

    @Override
    public ToDoListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_to_do, parent, false);

        return new ToDoListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final ToDoModel toDo = toDoList.get(position);

        holder.toDoTextView.setText(toDo.getToDo());
        holder.toDoKeyTextView.setText(toDo.getToDes());
        Glide.with(context).load(toDo.getImagePath())
                .into(holder.ivImage);
        holder.cbSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (toDo.getIsChecked()) {
                    toDo.setIsChecked(false);


                } else {
                    toDo.setIsChecked(true);

                }

                Toast.makeText(context, "selected :" +getSelectedList().size(), Toast.LENGTH_SHORT).show();

            }
        });


    }



    public ArrayList<ToDoModel> getSelectedList(){
        ArrayList<ToDoModel> toDoModels=new ArrayList<>();
        for (ToDoModel toDoModel: toDoList) {
            if(toDoModel.getIsChecked()){
                toDoModels.add(toDoModel);
            }

        }
        return toDoModels;
    }
    public void setToDoList(ArrayList<ToDoModel> toDoList) {
        this.toDoList = toDoList;
        filterList = toDoList;
    }

    public void changeItems(ArrayList<ToDoModel> toDoModel) {
        this.toDoList.clear();
        this.toDoList.addAll(toDoModel);
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (toDoList == null)
            return 0;
        return toDoList.size();
    }


    protected class ViewHolder extends RecyclerView.ViewHolder {
        private TextView toDoTextView;
        private TextView toDoKeyTextView;
        private Button deleteButton;
        private CheckBox cbSelect;
        private AppCompatImageView ivImage;

        public ViewHolder(View view) {
            super(view);
            toDoTextView = view.findViewById(R.id.tvTitle);
            toDoKeyTextView = view.findViewById(R.id.tvDes);
            deleteButton = view.findViewById(R.id.btn_delete);
            cbSelect = view.findViewById(R.id.cbSelect);
            ivImage = view.findViewById(R.id.ivImage);
            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteButtonClickListener.onDeleteClick(toDoList.get(getAdapterPosition()));
                    toDoList.remove(toDoList.get(getAdapterPosition()));
                    notifyDataSetChanged();
                }
            });
        }
    }

    private class RecordFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();


            if (constraint == null || constraint.length() == 0) {
                results.values = filterList;
                results.count = toDoList.size();

            } else {
                ArrayList<ToDoModel> fRecords = new ArrayList<ToDoModel>();

                for (ToDoModel s : filterList) {
                    if (s.getToDo().toLowerCase().trim().contains(constraint.toString().toLowerCase().trim())) {
                        fRecords.add(s);
                    }
                }
                results.values = fRecords;
                results.count = fRecords.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            toDoList = (ArrayList<ToDoModel>) results.values;
            notifyDataSetChanged();
        }
    }
}


