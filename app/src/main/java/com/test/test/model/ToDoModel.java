package com.test.test.model;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ToDoModel {

    @PrimaryKey(autoGenerate = true)
    private int toDoId;

    private String toDo;
    private String ImagePath;

    private Boolean isChecked=false;

    public Boolean getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(Boolean isChecked) {
        this.isChecked = isChecked;
    }

    public ToDoModel(String toDo, String toDes,String ImagePath) {
        this.toDo = toDo;
        this.toDes = toDes;
        this.ImagePath=ImagePath;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    public String getToDes() {
        return toDes;
    }

    public void setToDes(String toDes) {
        this.toDes = toDes;
    }

    private String toDes;



    public int getToDoId() {
        return toDoId;
    }

    public void setToDoId(int toDoId) {
        this.toDoId = toDoId;
    }

    public String getToDo() {
        return toDo;
    }

    public void setToDo(String toDo) {
        this.toDo = toDo;
    }
}
