package com.test.test.room;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.test.test.model.ToDoModel;

@Database(entities = {ToDoModel.class}, version = 1, exportSchema = false)

public abstract class AppDatabase extends RoomDatabase {

    //Add all created Dao here

    public abstract ToDoDao toDoDao();


}

