package com.test.test.room;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.test.test.model.ToDoModel;

import java.util.List;



@Dao
public interface ToDoDao {


    @Query("SELECT * FROM ToDoModel")
    List<ToDoModel> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveAll(List<ToDoModel> toDoList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void save(ToDoModel toDoModel);

    @Query("SELECT COUNT() FROM ToDoModel")
    int getNumberOfRows();

    @Query("DELETE FROM ToDoModel")
    void clearAll();

    @Delete
    void delete(ToDoModel toDoModel);


}
