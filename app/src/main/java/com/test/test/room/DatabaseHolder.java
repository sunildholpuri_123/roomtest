package com.test.test.room;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;


public class DatabaseHolder {
    private static AppDatabase DATABASE_INSTANCE;

    private static final String DATABASE_NAME = "test-database";


    public static AppDatabase getDatabaseInstance(@NonNull Context context) {

        if (DATABASE_INSTANCE == null) {
            DATABASE_INSTANCE = Room.databaseBuilder(
                    context.getApplicationContext(),
                    AppDatabase.class,
                    DATABASE_NAME
            ).build();
        }
        return DATABASE_INSTANCE;
    }

}
