package com.test.test;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.test.test.model.ToDoModel;
import com.test.test.room.DataBaseManager;

import java.util.ArrayList;



public class ToDoListActivity extends AppCompatActivity implements ToDoListAdapter.DeleteButtonClickListener {

    private ToDoListAdapter toDoListAdapter;
    private RecyclerView toDoRecyclerView;
    private DataBaseManager dataBaseManager;
    private TextView emptyListTextView;
    private EditText svFilter;
    private FloatingActionButton addFloatingActionButton;
    private ArrayList<ToDoModel> toDoModelArrayList;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataBaseManager = new DataBaseManager(this);
        setContentView(R.layout.activity_to_do_list);
        toDoRecyclerView = findViewById(R.id.rv_todo);
        svFilter = findViewById(R.id.svFilter);
        emptyListTextView = findViewById(R.id.tv_empty_list);
        addFloatingActionButton = findViewById(R.id.fab_add_todo);
        toDoListAdapter = new ToDoListAdapter(this, this);
        toDoModelArrayList = new ArrayList<>();

        svFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                toDoListAdapter.getFilter().filter(s.toString().toLowerCase());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }


        });

        addFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(ToDoListActivity.this,AddNoteActivity.class);
                startActivityForResult(intent, 2);// Activity is started with requestCode 2


            }
        });
        toDoRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        toDoRecyclerView.setAdapter(toDoListAdapter);
        ArrayList<ToDoModel> toDoModelArrayList = dataBaseManager.getData();

        if (toDoModelArrayList != null && toDoModelArrayList.size() > 0) {
            this.toDoModelArrayList.addAll(toDoModelArrayList);
            toDoListAdapter.setToDoList(toDoModelArrayList);
        }


    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==2)
        {
            if(data.getExtras()!=null){
            String title=data.getStringExtra("title");
            String des=data.getStringExtra("des");
            String image=data.getStringExtra("image");

            ToDoModel toDoModel = new ToDoModel(title,des,image);
            dataBaseManager.saveItemToDataBase(toDoModel);
            toDoListAdapter.changeItems(dataBaseManager.getData());}
        }
    }

    @Override
    public void onDeleteClick(ToDoModel toDoModel) {
        dataBaseManager.remove(toDoModel);
    }
}
